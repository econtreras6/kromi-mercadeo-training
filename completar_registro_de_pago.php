<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
     	<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- /////////////////////////////////////////CSS LIBRARIES/////////////////////////////////////////////////////////////////////////// -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
		<link rel="stylesheet" href="fondotraining.css">
   		<link href="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/d004434a5ff76e7b97c8b07c01f34ca69e635d97/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
   		<!-- /////////////////////////////////////////SCRIPTS LIBRARIES////////////////////////////////////////////////////////////////////// -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
		<script src="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/d004434a5ff76e7b97c8b07c01f34ca69e635d97/src/js/bootstrap-datetimepicker.js"></script>
		<meta http-equiv="Expires" content="0" /> 
		<meta http-equiv="Pragma" content="no-cache" />

		<script type="text/javascript">
		  if(history.forward(1)){
		    location.replace( history.forward(1) );
		  }
		</script>
	</head>
	<body>
		<div class="container">
			<br/><br/>
			<div>
				<img src="foto5.png" class="col-md-12" style="width:1137px;"/>
			</div>
			<label>&nbsp;&nbsp;&nbsp;&nbsp;<br/></label>
			<div class="col-xs-12">
				<form action="enviar.php" method="post"> 
					 <div class="panel panel-default" style="background-image: url('negro.jpg');">
					 	 <br/><br/><br/>
					 	 <div class="form-group">
			                <label class="col-xs-3" style="font: 120% sans-serif; color:white;">&nbsp;&nbsp;&nbsp;&nbsp;Banco de origen:</label>
			                <div class="col-xs-4 col-xs-pull-1">
			                    <select id="banco_orig" name="banco_orig" class="form-control" required></select>
			                </div>
			                <div class="form-group">
								<label style="font: 120% sans-serif; color:white;">
									  Transferencia&nbsp;&nbsp;&nbsp;  
								</label>
								<input type="radio" name="radio2" id="opcion1">
								<label style="font: 120% sans-serif; color:white;">
									  &nbsp;&nbsp;&nbsp;Deposito&nbsp;&nbsp;&nbsp;
								</label>
								 <input type="radio" name="radio2" id="opcion2" class="col-xs-offset-1">
						 	</div>
			            </div>
					 	<br/>
					 	<div class="form-group">
			                <label class="col-xs-4" style="font: 120% sans-serif; color:white;">&nbsp;&nbsp;&nbsp;&nbsp;Fecha de Transferencia:</label>
			                <div class="col-xs-3 col-xs-pull-1">
			                    <input id="fecha_transf" name="fecha_transf" type="date" class="form-control" required>
			                </div>
			                <label class="col-xs-2 col-xs-pull-1" style="font: 120% sans-serif; color:white;">Nro de Voucher:</label>
			                <div class="col-xs-3 col-xs-pull-1">
			                    <input id="voucher_nro" name="voucher_nro" type="number" class="form-control" required>
			                </div>
			            </div>
					 	<div id="ocultar">
					 		  <div class="col-xs-4">
			                    <input id="transf_online" name="transf_online" type="tel" class="form-control" required>
			                    <label class="col-xs-6" style="font: 130% sans-serif; color:orange;">Responda Si/No</label>
			                </div>
						 	 <div class="form-group">
				                <label class="col-xs-2 col-xs-offset-1" style="font: 120% sans-serif; color:white;">Nombres:</label>
				                <div class="col-xs-4 col-xs-pull-1">
				                    <input id="nombre" name="nombre" type="tel" class="form-control" readonly>
				                    <span id="errorNOMBRE" style="display: inline; font: 120% sans-serif;"></span>
				                </div>
				                <label class="col-xs-1 col-xs-pull-1" style="font: 120% sans-serif; color:white;">Apellidos:</label>
				                <div class="col-xs-4 col-xs-pull-1">
				                    <input id="apellido" name="apellido" type="tel" class="form-control" readonly>
				                    <span id="errorAPELLIDO" style="display: inline; font: 120% sans-serif;"></span>
				                </div>
				            </div>
						 	 <br/><br/><br/>
				            <div class="form-group">
				                <label class="col-xs-2 col-xs-offset-1" style="font: 120% sans-serif; color:white;">Cedula:</label>
				                <div class="col-xs-4 col-xs-pull-1">
				                    <input id="cedula" name="cedula" type="tel" class="form-control" readonly>
				                    <span id="errorCI" style="display: inline; font: 120% sans-serif;"></span>
				                </div>
				                <label class="col-xs-1 col-xs-pull-1" style="font: 120% sans-serif; color:white;">Correo:</label>
				                <div class="col-xs-4 col-xs-pull-1">
				                    <input id="email" name="email" type="tel" class="form-control" readonly>
				                    <span id="errorEMAIL" style="display: inline; font: 120% sans-serif;"></span>
				                </div>
				            </div>
				            <br/><br/><br/><br/>

				            <div class="form-group">
				                <label class="col-xs-2 col-xs-offset-1" style="font: 120% sans-serif; color:white;">Fecha de Nacimiento:</label>
				                <div class="col-xs-3">
				                    <input id="fech_nac" name="fech_nac" type="tel" class="form-control" readonly>
				                    <span id="errorFECH_NAC" style="display: inline; font: 120% sans-serif;"></span>
				                </div>
				                <label class="col-xs-1" style="font: 120% sans-serif; color:white;">Sexo:</label>
				                <div class="col-xs-4">
				                	<input id="sexo" name="sexo" type="tel" class="form-control" readonly>
				                    <span id="errorSEXO" style="display: inline; font: 120% sans-serif;"></span>
				                </div>
				                <span id="errorSEXO" style="display: inline; font: 120% sans-serif;"></span>
				            </div>
				            <br/><br/><span id="espacios"></span><br/>

				            <div class="form-group">
				                <label class="col-xs-1 col-xs-offset-1" style="font: 120% sans-serif; color:white;">Ocupacion:</label>
				                <div class="col-xs-3 col-xs-offset-1">
				                    <input id="ocupacion" name="ocupacion" type="tel" class="form-control" readonly>
				                    <span id="errorOCUPACION" style="display: inline; font: 120% sans-serif;"></span>
				                </div>
				                <label class="col-xs-2" style="font: 120% sans-serif; color:white;">Zona de residencia:</label>
				                <div class="col-xs-3">
				                	<input id="zona_res" name="zona_res" type="tel" class="form-control" readonly>
				                    <span id="errorZONA_RES" style="display: inline; font: 120% sans-serif;"></span>
				                </div>
				            </div>
				            <br/><br/><br/><br/>><br/>
				            <div class="form-group">
				                <label class="col-xs-1 col-xs-offset-1" style="font: 120% sans-serif; color:white;">Telefono contacto:</label>
				                <div class="col-xs-3  col-xs-offset-1">
				                    <input id="tlf_residencia" name="tlf_residencia" type="tel" class="form-control"  readonly>
				                    <span id="errorTELEFONO" style="display: inline; font: 120% sans-serif;"></span>
				                </div>
								<label class="col-xs-2" style="font: 120% sans-serif; color:white;">Monto a pagar:</label>
				                <div class="col-xs-3">
				                	<input id="monto" name="monto" type="tel" class="form-control" readonly>
				                    <span id="errorZONA_RES" style="display: inline; font: 120% sans-serif;"></span>
				                </div>
				            </div>
				            <br/><br/><br/><br/>><br/>
				            <div class="form-group">
				                <label class="col-xs-1 col-xs-offset-1" style="font: 120% sans-serif; color:white;">Sesion1:</label>
				                <div class="col-xs-3  col-xs-offset-1">
				                    <input id="Sesion1" name="Sesion1" type="tel" class="form-control"  readonly>
				                    <span id="errorTELEFONO" style="display: inline; font: 120% sans-serif;"></span>
				                </div>
								<label class="col-xs-2" style="font: 120% sans-serif; color:white;">Sesion2:</label>
				                <div class="col-xs-3">
				                	<input id="Sesion2" name="Sesion2" type="tel" class="form-control" readonly>
				                    <span id="errorZONA_RES" style="display: inline; font: 120% sans-serif;"></span>
				                </div>
				            </div>
				            <br/><br/><br/><br/>><br/>
				            <div class="form-group">
				                <label class="col-xs-1 col-xs-offset-1" style="font: 120% sans-serif; color:white;">Sesion3:</label>
				                <div class="col-xs-3  col-xs-offset-1">
				                    <input id="Sesion3" name="Sesion3" type="tel" class="form-control"  readonly>
				                    <span id="errorTELEFONO" style="display: inline; font: 120% sans-serif;"></span>
				                </div>
								<label class="col-xs-2" style="font: 120% sans-serif; color:white;">Sesion4:</label>
				                <div class="col-xs-3">
				                	<input id="Sesion4" name="Sesion4" type="tel" class="form-control" readonly>
				                    <span id="errorZONA_RES" style="display: inline; font: 120% sans-serif;"></span>
				                </div>
				            </div>
				            <br/><br/><br/><br/>
						 </div>
						 <br/><br/><br/>
					 	<table class="table">
							<tr>
								<td class="col-xs-4">
								</td>
								<td class="col-xs-6">
									<button  id="enviar" name="enviar" type="submit" class="col-xs-offset-2 col-xs-4" style="font: 120% sans-serif; color:gray;">Enviar</button>
								</td>
								<td class="col-xs-4">
								</td>
							</tr>
						</table>
						<br/><br/>
					</div>
				</form>
			</div>
			<div>
				<img src="foto1.png" class="col-md-12" style="width:1137px;"/>
			</div>
			<br/><br/><br/>
			<label>&nbsp;&nbsp;&nbsp;&nbsp;</label>
		</div>
	</body>
	<script  type="text/javascript">
		var cont1 = 0,cont2 = 0;
		var URL = String(window.location);
		arregloDeSubCadenas = URL.split("?CI=");
		

		$('#enviar').click(function(){
			if($("#voucher_nro").val() != "" && $("#banco_orig").val() != "" && $("#fecha_transf").val() != "" && $("#transf_online").val() != "")
				var from = $('#fecha_transf').val().split("-");
			    fecha = new Date(from[2],from[1]-1,from[0]);
			    cadena = fecha.getDate() + "/" + (fecha.getMonth() +1) + "/" + fecha.getFullYear();

				$.ajax({
						data: {"ci":arregloDeSubCadenas[1],"nro_voucher":$("#voucher_nro").val(),"banco_orig":$("#banco_orig").val(),"fecha_transf":cadena,"transf_online":$("#transf_online").val()},
						url:   'registrar_pago.php',
						type:  'post',
						dataType: 'json',
						success:  function (response) {
							alert(response);
						}	
				});
		});

		$('#opcion1').click(function(){
			var opcion1 = $('input:radio[id=opcion1]:checked').val();
			if(opcion1 == "on")
				$("#transf_online").val("si");
		});

		$('#opcion2').click(function(){
			var opcion2 = $('input:radio[id=opcion2]:checked').val();
			if(opcion2 == "on")
				$("#transf_online").val("no");
		});

		$(document).ready(function(){
			$('#ocultar').hide();
			$('#monto').val(arregloDeSubCadenas[2]);
			$.ajax({
					url:   'mandar_bancos.php',
					type:  'post',
					dataType: 'json',
					success:  function (response) {
						cadena = "";
						for(i = 1; i < response[0]; i++)
							cadena = cadena + "<option>"+response[i]+"</option>";
						$('#banco_orig').html(cadena);
					}
				});

			$.ajax({
					data: {"ci":arregloDeSubCadenas[1]},
					url:   'mandar_datos.php',
					type:  'post',
					dataType: 'json',
					success:  function (response) {
						cadena="";
						$('#nombre').val(response[0]);
						$('#apellido').val(response[1]);
						$('#cedula').val(response[2]);
						$('#email').val(response[3]);
						if(response[4] == 'M')
							$('#sexo').val("Masculino");
						else
							$('#sexo').val("Femenino");
						$('#fech_nac').val(response[5]);
						$('#ocupacion').val(response[8]);
						$('#zona_res').val(response[6]);
						$('#tlf_residencia').val(response[7]); 
						for(i = 9; i < response.length; i++)
						{
							if(response[i] != "")
							{
								j = i-8;
								cadena = cadena+"#Sesion"+j;
								$(cadena).val(response[i]);	
								cadena="";
							}	
						}
					}
				});
		});
	</script>
