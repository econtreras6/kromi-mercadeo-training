<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
     	<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- /////////////////////////////////////////CSS LIBRARIES/////////////////////////////////////////////////////////////////////////// -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
		<link rel="stylesheet" href="fondotraining.css">
   		<link href="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/d004434a5ff76e7b97c8b07c01f34ca69e635d97/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
   		<!-- /////////////////////////////////////////SCRIPTS LIBRARIES////////////////////////////////////////////////////////////////////// -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
		<script src="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/d004434a5ff76e7b97c8b07c01f34ca69e635d97/src/js/bootstrap-datetimepicker.js"></script>
		<meta http-equiv="Expires" content="0" /> 
		<meta http-equiv="Pragma" content="no-cache" />

		<script type="text/javascript">
		  if(history.forward(1)){
		    location.replace( history.forward(1) );
		  }
		</script>
	</head>
	<body>
		<div class="container">
			<br/><br/>
			<div>
				<img src="foto5.png" class="col-md-12" style="width:1100px;"/>
				<label>&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/><br/></label>
			</div>
			<div>
				<img src="foto2.png" class="col-md-12" style="width:1100px;"/>
			</div>
			<label>&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/><br/></label>
			<div class="col-xs-12" style="width:1100px;">
				<div class="panel panel-default" style="background-image: url('negro.jpg'); border-image: url('negro.jpg');">
					<br/><br/><br/><br/>
					<table>
						<tr>
							<td><label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label></td>
							<td>
								<p style="font: 120% sans-serif; color:white; text-align: justify;">En este momento estamos tramitando tu pago. En las próximas 72 horas (hábiles) confirmaremos tu transacción mediante un correo electrónico.<br/><br/>

								Nota: Si una vez transcurridas 72 horas no has recibido el correo de confirmación, verifica nuevamente tu correo electronico y haz Click en enviar o comunícate al 0241-2004836 (en horario de oficina).<br/><br/></p>
								<br/>
<hr/>
<br/>
<p style="font: 120% sans-serif; color:white; text-align: justify;">Visita nuestras redes sociales <a href="https://www.twitter.com/kromimarket" target="_blank"><img src="twitter.png"/></a><a href="https://www.instagram.com/kromimarket" target="_blank"><img src="instagram.png"/></a><a href="https://www.facebook.com/Kromi-Market-367896906610761/?fref=ts" target="_blank"><img src="facebook.png"/></a><p>
<p style="font: 120% sans-serif; color:white; text-align: justify;">Escucha la mejor música seleccionada para ti en nuestro Playlist <a href="https://www.youtube.com/channel/UCeYZ-o4SXTJTAzJ-QnnruiQ" target="_blank"><img src="youtube.png"/></a><br/><br/></p>
<hr/>
<br/>
								<button  id="inscribirse" type="button" class="col-xs-offset-4 col-xs-4" style="font: 120% sans-serif; color:white; background:gray;">Verificar Datos</button>
							</td>
							<td><label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label></td>
						</tr>
					</table>
					<br/><br/>
					<label>&nbsp;&nbsp;&nbsp;&nbsp;</label>
				</div>
			</div>
	</body>
	<script  type="text/javascript">
		var URL = String(window.location);
		arregloDeSubCadenas = URL.split("?CI=");

		$("#inscribirse").click(function(){
			window.location="verificar_correo.php?CI="+arregloDeSubCadenas[1]+"?CI="+arregloDeSubCadenas[2];
		});
	</script>
