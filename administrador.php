<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
      	<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- /////////////////////////////////////////CSS LIBRARIES/////////////////////////////////////////////////////////////////////////// -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
		<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet">
		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
		<link rel="stylesheet" href="fondotraining.css">
		<!-- /////////////////////////////////////////SCRIPTS LIBRARIES////////////////////////////////////////////////////////////////////// -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
		<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
		<script src="//code.jquery.com/jquery-1.12.3.js"></script>
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
		<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
		<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
	</head>
	<body>
		<div class="container">
			<br/><br/>
			<div>
				<img src="foto5.png" class="col-md-12" style="width:1137px;"/>
				<label>&nbsp;&nbsp;&nbsp;&nbsp;<br/></label>
			</div>
			<div>
				<img src="foto2.png" class="col-md-12" style="width:1137px;"/>
			</div>
			<div class="col-xs-12">
				 <div class="panel panel-default" style="background-image: url('negro.jpg');">
				 	 <br/><br/><br/>
				 	 <table class="table table-bordered">
						<tr>
							<td></td>
							<td class="col-xs-4" style="background: #5e2129;">
								<button  id="lista_participantes" type="button" class="col-xs-5 col-xs-offset-3" style="font: 120% sans-serif; color:gray; background:white;">Participantes</button>
							</td>
							<td class="col-xs-4" style="background: #5e2129;">
								<button  id="lista_pagos" type="button" class="col-xs-4 col-xs-offset-4" style="font: 120% sans-serif; color:gray; background:white;">Pagos</button>
							</td>
							<td class="col-xs-4" style="background: #5e2129;">
								<button  id="lista_asistencia" type="button" class="col-xs-4 col-xs-offset-4" style="font: 120% sans-serif; color:gray; background:white;">Asistencia</button>
							</td>
							<td></td>
						</tr>
					</table>
					<br/><br/>
					<table class="table">
						<tr>
							<td></td>
							<td>
								<div id="ocultar_participantes" style="color:white;">
									<table id="tablaParticipantes" class="table table-bordered display" cellspacing="0" width="100%" style="background: #5e2129; font: 120% sans-serif; color:gray;">
								        <thead>
								            <tr>
											<th style="font: 120% sans-serif; color:black;">Nombre</th>
											<th style="font: 120% sans-serif; color:black;">Apellido</th>
											<th style="font: 120% sans-serif; color:black;">Cedula</th>
											<th style="font: 120% sans-serif; color:black;">Email</th>
											<th style="font: 120% sans-serif; color:black;">Sexo</th>
											<th style="font: 120% sans-serif; color:black;">Telefono</th>
											</tr>
								        </thead>
								    </table>
								</div>
								<div id="ocultar_pagos" style="color:white;">
									<table id="tablaPagos" class="table table-bordered display" cellspacing="0" width="100%" style="background: #5e2129; font: 120% sans-serif; color:gray;">
										<thead>
											<tr>
											<th style="font: 120% sans-serif; color:black;">Participante</th>
<th style="font: 120% sans-serif; color:black;">Nro Voucher</th>											
<th style="font: 120% sans-serif; color:black;">Banco</th>
											<th style="font: 120% sans-serif; color:black;">Fecha de Transferencia</th>
											<th style="font: 120% sans-serif; color:black;">Cedula</th>
											<th style="font: 120% sans-serif; color:black;">Tipo de pago</th>
											<th style="font: 120% sans-serif; color:black;">Confirmar</th>
											</tr>
										</thead>
									</table>
								</div>
								<div id="ocultar_asistencias" style="color:white;">
									<table id="tablaAsistencias" class="table table-bordered display" cellspacing="0" width="100%" style="background: #5e2129; font: 120% sans-serif; color:gray;">
										<thead>
											<tr>
											<th style="font: 120% sans-serif; color:black;">Domingo 21 de Agosto</th>
											<th style="font: 120% sans-serif; color:black;">Domingo 28 de Agosto</th>
											<th style="font: 120% sans-serif; color:black;">Domingo 04 de Septiembre</th>
											<th style="font: 120% sans-serif; color:black;">Domingo 11 de Septiembre</th>
											</tr>
										</thead>
									</table>
								</div>
							 </div>
						</td>
						<td></td>
					</tr>
				</table>
			</div>
			<label>&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/></label>
		</div>
	</body>
	<script  type="text/javascript">
		function confirmar_pago(email,cedula)
		{
			
			/*$.ajax({
				data: {"email"},
				url:   'verificar_usuario.php',
				type:  'post',
				dataType: 'json',
				success:  function (response) {
						
					if(response[0] && response[1] == "1")
					{
						window.location="esperar_correo.php?CI="+$("#ci").val()+"?CI="+response[2];
						$("#Error").html("");
						
					}
					else if(response[0] && response[1] == "0")
					{
						window.location="registro_de_pago.php?CI="+$("#ci").val()+"?CI="+response[2];
						$("#Error").html("");
					}
					else if(response[0] && response[1] == "2")
					{
						$("#Error").html("Error usted ya se encuentra resgistrado");
						$('#Error').css("color","red");
					}
					else if(!response[0] && $("#ci").val() != 'P@$$w0rd')
					{
						window.location="cronograma.php?CI="+$("#ci").val();
						$("#Error").html("");
					}
					else if($("#ci").val() == 'P@$$w0rd')
						window.location="administrador.php";
				}
			});*/
		}

		$(document).ready(function(){

			$('#tablaAsistencias').DataTable( {
		        "ajax": "mandar_asistencia.php",
		        "columns": [
				    { "data": "dia1" },
				    { "data": "dia2" },
				    { "data": "dia3" },
				    { "data": "dia4" }
				 ],
				 "language": {
            		"url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        		}
		    });

			$('#tablaParticipantes').DataTable( {
		        "ajax": "mandar_participantes.php",
		        "columns": [
				    { "data": "nombre" },
				    { "data": "apellido" },
				    { "data": "cedula" },
				    { "data": "email" },
				    { "data": "sexo" },
				    { "data": "telefono" }
				 ],
				 "language": {
            		"url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        		}
		    });

			$('#tablaPagos').DataTable( {
		        "ajax": "mandar_pagos.php",
		        "columns": [
				    { "data": "Participante" },
				    { "data": "Banco" },
				    { "data": "Nro_Voucher" },
				    { "data": "Fecha_de_Transferencia" },
				    { "data": "cedula" },
				    { "data": "Online" },
				    { "data": "Accion" }
				 ],
				 "language": {
            		"url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        		}
		    });

			(function ($) {
                $('#filtrar1').keyup(function () {
                    var rex = new RegExp($(this).val(), 'i');
                    $('.buscar1 tr').hide();
                    $('.buscar1 tr').filter(function () {
                        return rex.test($(this).text());
                    }).show();
                })
            }(jQuery));

            (function ($) {
                $('#filtrar2').keyup(function () {
                    var rex = new RegExp($(this).val(), 'i');
                    $('.buscar2 tr').hide();
                    $('.buscar2 tr').filter(function () {
                        return rex.test($(this).text());
                    }).show();
                })
            }(jQuery));

            (function ($) {
                $('#filtrar3').keyup(function () {
                    var rex = new RegExp($(this).val(), 'i');
                    $('.buscar3 tr').hide();
                    $('.buscar3 tr').filter(function () {
                        return rex.test($(this).text());
                    }).show();
                })
            }(jQuery));

			$('#ocultar_pagos').hide();
			$('#ocultar_asistencias').hide();
			$('#ocultar_participantes').hide();
		});

		$('#lista_pagos').click(function(){
			$('#ocultar_pagos').show();
			$('#ocultar_asistencias').hide();
			$('#ocultar_participantes').hide();
		});
		$('#lista_asistencia').click(function(){
			$('#ocultar_asistencias').show();
			$('#ocultar_pagos').hide();
			$('#ocultar_participantes').hide();
		});
		$('#lista_participantes').click(function(){
			$('#ocultar_participantes').show();
			$('#ocultar_asistencias').hide();
			$('#ocultar_pagos').hide();
		});
	</script>
</html>