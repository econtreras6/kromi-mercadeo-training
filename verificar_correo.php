<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
     	<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- /////////////////////////////////////////CSS LIBRARIES/////////////////////////////////////////////////////////////////////////// -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
		<link rel="stylesheet" href="fondotraining.css">
   		<link href="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/d004434a5ff76e7b97c8b07c01f34ca69e635d97/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
   		<!-- /////////////////////////////////////////SCRIPTS LIBRARIES////////////////////////////////////////////////////////////////////// -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
		<script src="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/d004434a5ff76e7b97c8b07c01f34ca69e635d97/src/js/bootstrap-datetimepicker.js"></script>
		<meta http-equiv="Expires" content="0" /> 
		<meta http-equiv="Pragma" content="no-cache" />

		<script type="text/javascript">
		  if(history.forward(1)){
		    location.replace( history.forward(1) );
		  }
		</script>
	</head>
	<body>
		<div class="container">
			<br/><br/>
			<div>
				<img src="foto5.png" class="col-md-12" style="width:1137px;"/>
			</div>
			<label>&nbsp;&nbsp;&nbsp;&nbsp;<br/></label>
			<div class="col-xs-12">
				<form  action="enviar_confirmacion.php" method="post"> 
					 <div class="panel panel-default" style="background-image: url('negro.jpg');">
					 	 <br/><br/><br/>
					 	 <div class="form-group">
			                <label class="col-xs-2 col-xs-offset-1" style="font: 120% sans-serif; color:white;">Nombres:</label>
			                <div class="col-xs-4 col-xs-pull-1">
			                    <input id="nombre" name="nombre" type="tel" class="form-control" readonly>
			                    <span id="errorNOMBRE" style="display: inline; font: 120% sans-serif;"></span>
			                </div>
			                <label class="col-xs-1 col-xs-pull-1" style="font: 120% sans-serif; color:white;">Apellidos:</label>
			                <div class="col-xs-4 col-xs-pull-1">
			                    <input id="apellido" name="apellido" type="tel" class="form-control" readonly>
			                    <span id="errorAPELLIDO" style="display: inline; font: 120% sans-serif;"></span>
			                </div>
			            </div>
			            <br/><br/><br/>

			            <div class="form-group">
			                <label class="col-xs-2 col-xs-offset-1" style="font: 120% sans-serif; color:white;">Cédula:</label>
			                <div class="col-xs-4 col-xs-pull-1">
			                    <input id="cedula" name="cedula" type="tel" class="form-control" readonly>
			                    <span id="errorCI" style="display: inline; font: 120% sans-serif;"></span>
			                </div>
			                <label class="col-xs-1 col-xs-pull-1" style="font: 120% sans-serif; color:white;">Correo:</label>
			                <div class="col-xs-4 col-xs-pull-1">
			                    <input id="email" name="email" type="tel" class="form-control" required>
			                    <span id="errorEMAIL" style="display: inline; font: 120% sans-serif;"></span>
			                </div>
			            </div>
			            <br/><br/><br/><br/>

			            <div class="form-group">
			                <label class="col-xs-2 col-xs-offset-1" style="font: 120% sans-serif; color:white;">Fecha de Nacimiento:</label>
			                <div class="col-xs-3">
			                    <div class="input-group date" id="datetimepicker">
			                        <input id="fech_nac" name="fech_nac" type="tel" class="form-control" readonly>
			                        <span class="input-group-addon">
			                            <span class="glyphicon glyphicon-calendar"></span>
			                        </span>
			                    </div>
			                    <span id="errorFECH_NAC" style="display: inline; font: 120% sans-serif;"></span>
			                </div>
			                <label class="col-xs-1" style="font: 120% sans-serif; color:white;">Sexo:</label>
			                <div class="col-xs-4">
			                    <input class="form-control" name="sexo" id="sexo" readonly>
			                    <span id="errorSEXO" style="display: inline; font: 120% sans-serif;"></span>
			                </div>
			                <span id="errorSEXO" style="display: inline; font: 120% sans-serif;"></span>
			            </div>
			            <br/><br/><span id="espacios"></span><br/>

			            <div class="form-group">
			                <label class="col-xs-1 col-xs-offset-1" style="font: 120% sans-serif; color:white;">Ocupación:</label>
			                <div class="col-xs-3 col-xs-offset-1">
			                    <input id="ocupacion" name="ocupacion" type="tel" class="form-control" readonly>
			                    <span id="errorOCUPACION" style="display: inline; font: 120% sans-serif;"></span>
			                </div>
			                <label class="col-xs-2" style="font: 120% sans-serif; color:white;">Zona de residencia:</label>
			                <div class="col-xs-3">
			                	<input id="zona_res" name="zona_res" type="tel" class="form-control" readonly>
			                    <span id="errorZONA_RES" style="display: inline; font: 120% sans-serif;"></span>
			                </div>
			            </div>
			            <br/><br/><br/><br/>><br/>
			            <div class="form-group">
			                <label class="col-xs-1 col-xs-offset-1" style="font: 120% sans-serif; color:white;">Teléfono contacto:</label>
			                <div class="col-xs-3  col-xs-offset-1">
			                    <input id="tlf_residencia" name="tlf_residencia" type="tel" class="form-control" readonly>
			                </div>
			                <br/><br/>
			                <span id="errorTELEFONO" style="display: inline; font: 120% sans-serif;" class="col-xs-offset-1"></span>
			            </div>
						<div id="ocultar">
							<br/><br/><br/>
						 	 <div class="form-group">
				                <label class="col-xs-3" style="font: 120% sans-serif; color:white;">&nbsp;&nbsp;&nbsp;&nbsp;Banco de origen:</label>
				                <div class="col-xs-4 col-xs-pull-1">
				                    <input id="banco_orig" name="banco_orig" class="form-control">
				                </div>
				            </div>
						 	<br/>
						 	<div class="form-group">
				                <label class="col-xs-4" style="font: 120% sans-serif; color:white;">&nbsp;&nbsp;&nbsp;&nbsp;Fecha de Transferencia:</label>
				                <div class="col-xs-3 col-xs-pull-1">
				                    <input id="fecha_transf" name="fecha_transf" type="date" class="form-control">
				                </div>
				                <label class="col-xs-2 col-xs-pull-1" style="font: 120% sans-serif; color:white;">Nro de Voucher:</label>
				                <div class="col-xs-3 col-xs-pull-1">
				                    <input id="voucher_nro" name="voucher_nro" type="number" class="form-control">
				                </div>
				            </div>
					 		  <div class="col-xs-4">
			                    <input id="transf_online" name="transf_online" type="tel" class="form-control">
								<label class="col-xs-2" style="font: 120% sans-serif; color:white;">Monto a pagar:</label>
				                <div class="col-xs-3">
				                	<input id="monto" name="monto" type="tel" class="form-control" readonly>
				                    <span id="errorZONA_RES" style="display: inline; font: 120% sans-serif;"></span>
				                </div>
				            </div>
				            <br/><br/><br/><br/>><br/>
				            <div class="form-group">
				                <label class="col-xs-1 col-xs-offset-1" style="font: 120% sans-serif; color:white;">Sesion1:</label>
				                <div class="col-xs-3  col-xs-offset-1">
				                    <input id="Sesion1" name="Sesion1" type="tel" class="form-control"  readonly>
				                    <span id="errorTELEFONO" style="display: inline; font: 120% sans-serif;"></span>
				                </div>
								<label class="col-xs-2" style="font: 120% sans-serif; color:white;">Sesion2:</label>
				                <div class="col-xs-3">
				                	<input id="Sesion2" name="Sesion2" type="tel" class="form-control" readonly>
				                    <span id="errorZONA_RES" style="display: inline; font: 120% sans-serif;"></span>
				                </div>
				            </div>
				            <br/><br/><br/><br/>><br/>
				            <div class="form-group">
				                <label class="col-xs-1 col-xs-offset-1" style="font: 120% sans-serif; color:white;">Sesion3:</label>
				                <div class="col-xs-3  col-xs-offset-1">
				                    <input id="Sesion3" name="Sesion3" type="tel" class="form-control"  readonly>
				                    <span id="errorTELEFONO" style="display: inline; font: 120% sans-serif;"></span>
				                </div>
								<label class="col-xs-2" style="font: 120% sans-serif; color:white;">Sesion4:</label>
				                <div class="col-xs-3">
				                	<input id="Sesion4" name="Sesion4" type="tel" class="form-control" readonly>
				                    <span id="errorZONA_RES" style="display: inline; font: 120% sans-serif;"></span>
				                </div>
				            </div>
				            <br/><br/><br/><br/>
						 </div>
			            <br/><br/><br/><br/>
					 	 <table class="table">
							<tr>
								<td class="col-xs-4">
								</td>
								<td class="col-xs-6">
									<button  id="guardar" type="submit" class="col-xs-offset-2 col-xs-4" style="font: 120% sans-serif; color:gray;">Enviar</button>
								</td>
								<td class="col-xs-4">
								</td>
							</tr>
						</table>
				         <br/><br/><br/>
					 </div>
				</form>
			</div>
			<div>
				<img src="foto1.png" class="col-md-12" style="width:1137px;"/>
			</div>
			<br/><br/><br/>
			<label>&nbsp;&nbsp;&nbsp;&nbsp;</label>
		</div>
	</body>
	<script  type="text/javascript">
		var URL = String(window.location);
		arregloDeSubCadenas = URL.split("?CI=");

		$(document).ready(function(){
			$('#monto').val(arregloDeSubCadenas[2]);
			$('#ocultar').hide();
			$.ajax({
					data: {"ci":arregloDeSubCadenas[1]},
					url:   'mandar_datos.php',
					type:  'post',
					dataType: 'json',
					success:  function (response) {
						cadena="";
						$('#nombre').val(response[0]);
						$('#apellido').val(response[1]);
						$('#cedula').val(response[2]);
						$('#email').val(response[3]);
						$('#sexo').val(response[4]);
						$('#fech_nac').val(response[5]);
						$('#ocupacion').val(response[8]);
						$('#zona_res').val(response[6]);
						$('#tlf_residencia').val(response[7]); 
						for(i = 9; i < response.length; i++)
						{
							if(response[i] != "")
							{
								j = i-8;
								cadena = cadena+"#Sesion"+j;
								$(cadena).val(response[i]);	
								cadena="";
							}	
						}
					}
				});
			$.ajax({
					data: {"ci":arregloDeSubCadenas[1]},
					url:   'mandar_datos_banco.php',
					type:  'post',
					dataType: 'json',
					success:  function (response) {
						$('#banco_orig').val(response.banco_orig);
						$('#transf_online').val(response.transf_online);
						$('#fecha_transf').val(response.fecha_transf);
						$('#voucher_nro').val(response.nro_voucher);
					}
				});
		});

		$("#guardar").click(function(){
			var from = $('#fech_nac').val().split("/");
		    fecha = new Date(from[2],from[1]-1,from[0]);
		    cadena = fecha.getDate() + "/" + (fecha.getMonth() +1) + "/" + fecha.getFullYear();

			$.ajax({
					data: {"nombre":$('#nombre').val(),"apellido":$('#apellido').val(),"email":$('#email').val(),"fecha_nac":cadena,"zona_res":$('#zona_res').val(),"tlf_residencia":$('#tlf_residencia').val(),"cedula":arregloDeSubCadenas[1],"ocupacion":$('#ocupacion').val(),"sexo":$('#sexo').val()},
					url:   'actualizar_usuario.php',
					type:  'post',
					dataType: 'json',
					success:  function (response) {
					}
				});
		});
	</script>