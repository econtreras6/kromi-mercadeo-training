<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
      	<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- /////////////////////////////////////////CSS LIBRARIES/////////////////////////////////////////////////////////////////////////// -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
		<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet">
		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
		<link rel="stylesheet" href="fondotraining.css">
		<!-- /////////////////////////////////////////SCRIPTS LIBRARIES////////////////////////////////////////////////////////////////////// -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
		<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
	</head>
	<body>
<div id="myModal2" class="modal fade" role="dialog"  data-keyboard="false" data-backdrop="static">
						  <div class="modal-dialog">

						    <!-- Modal content-->
						    <div class="modal-content  alert alert-danger alert-dismissable">
						        <div style="width: 570px; height: 200px;">
						        	<div class="form-group">
						                <div class="col-xs-5">
						                   	<img src="error.png" style="width: 160px; height: 130px;" class="col-xs-offset-1"/>
						                </div>
						                <div class="col-xs-7">
						                    <br/><h3><strong>¡Error!</strong> No se puede visualizar esta pagina en el navegador FIREFOX.</h3>
						                </div>
						          	</div>
						          	<button  id="cerrar2" type="button" class="btn btn-default pull-right col-md-3" data-dismiss="modal">Cerrar</button>
						        </div>
						    </div>

						  </div>
						</div>

		<div class="container">
			<br/><br/>
			<div>
				<img src="foto5.png" class="col-md-12" style="width:1100px;"/>
				<label>&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/><br/><br/><br/><br/><br/><br/></label>
			</div>
			<div>
				<img src="foto2.png" class="col-md-12" style="width:1100px;"/>
			</div>
			<label>&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/></label>
			<div id="ocultar_informacion">
				<div class="col-xs-12" style="width:1100px;">
					<div class="panel panel-default" style="background-image: url('negro.jpg'); border-image: url('negro.jpg');">
						<br/>
						<table>
							<tr>
								<td><label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label></td>
								<td>
									<p style="font: 120% sans-serif; color:white; text-align: justify;">En Agosto y Septiembre: Kromi Training<br/><br/>
									Los días 21, 28 de agosto y 04, 11 de septiembre en Kromi Market Mañongo, desde las 7:00am: Actividades físicas, deportivas y recreativas serán las protagonistas de nuestros domingos, a beneficio de la Fundación Llevamos Alegría.<br/><br/>

									De la mano de X-Training Fitness, una empresa que desarrolla programas de entrenamiento personalizado en pro de la buena salud física, psíquica y funcional de nuestros cuerpos, los participantes pondrán a prueba su resistencia y desempeño físico en las siguientes disciplinas:<br/><br/>

									*Entrenamiento Funcional<br/>
									*GAP (Glúteos, abdomen y piernas)<br/>
									*GetFit Max (autocarga)<br/>
									*Entrenamiento Familiar (Zumba Fitness)<br/><br/>

									Si estas interesado en activar tu cuerpo y darle ese BOOM de energía que necesita, ¡Inscríbete! Sin duda será un evento donde atletas certificados te mostrarán lo provechoso que es el ejercicio para tu cuerpo y encontrarás la inspiración para seguir entrenando y mejorando tu salud física.</p>
									<br/><br/>
									<button  id="inscribirse" type="button" class="col-xs-offset-4 col-xs-4" style="font: 120% sans-serif; color:white; background:gray;">Inscribirse</button>
								</td>
								<td><label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label></td>
							</tr>
						</table>
						<br/><br/>
						<label>&nbsp;&nbsp;&nbsp;&nbsp;</label>
					</div>
				</div>
			</div>
			<div class="col-xs-4">
				<div id="ocultar">
					<form>
						<div class="form-group" style="width:1260px;">
							<div class="panel panel-default col-xs-5" style="width:540px; background-image: url('blanco.jpg'); border-image: url('blanco.jpg');">
								<div class="form-group col-xs-offset-1">
									<br/><br/><br/><br/><br/><br/><br/>
									<label class="col-xs-11" style="font: 120% sans-serif; color:white;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;INTRODUCE TU NÚMERO DE CÉDULA</label>
						            <div class="col-xs-11">
						                <input id="ci" type="tel" class="form-control">
						                <span id="Error" class="col-xs-12" style="display: inline; font: 120% sans-serif; color:white;"></span>
						            </div>
								</div>
								<br/><br/><br/>
								<button  id="entrar" type="button" class="col-xs-offset-4 col-xs-4" style="font: 120% sans-serif; color:white; background:gray;">Entrar</button>
								<br/><br/><br/><br/><br/><br/><br/><br/>
								<label>&nbsp;&nbsp;&nbsp;&nbsp;</label>
							</div>
							<div class="panel panel-default col-xs-2" style="width:530px;  background-image: url('negro.jpg'); border-image: url('negro.jpg');">
								<br/>
								<label class="col-xs-9" style="font: 120% sans-serif; color:white;">Información General:</label>
								<br/><br/>
								<label class="col-xs-12" style="font: 100% sans-serif; color:white;"><span style="color:green;"><strong>✓</strong></span> Actividad para mayores de 18 años.</label>
								<br/><br/>
								<label class="col-xs-12" style="font: 100% sans-serif; color:white;"><span style="color:green;"><strong>✓</strong></span>  El tiempo de duración de cada sesión es de 90 mins.</label>
								<br/><br/>
								<label class="col-xs-12" style="font: 100% sans-serif; color:white;"><span style="color:green;"><strong>✓</strong></span>  La inscripción es individual e intransferible.</label>
								<br/><br/>
								<label class="col-xs-12" style="font: 100% sans-serif; color:white;"><span style="color:green;"><strong>✓</strong></span>  Los pagos serán realizados Vía Transferencia y/o Depósito.</label>
								<br/><br/>
								<label class="col-xs-12" style="font: 100% sans-serif; color:white;"><span style="color:green;"><strong>✓</strong></span>  Para la actividad familiar del Domingo 11/09 los niños deben ser<br/>&nbsp;&nbsp;&nbsp;mayores de 12 años.</label>
								<br/><br/><br/>
								<label class="col-xs-12" style="font: 100% sans-serif; color:white;"><span style="color:green;"><strong>✓</strong></span>  Revisa bien tus opciones antes de dar Click en Siguiente: No<br/>&nbsp;&nbsp;&nbsp;realizamos reintegros.</label>
								<br/><br/><br/>
                                                                <label class="col-xs-12" style="font: 100% sans-serif; color:white;"><span style="color:green;"><strong>✓</strong></span>  Para ésta actividad debes mantenerte hidratado, por lo que un cooler<br/>&nbsp;&nbsp;&nbsp;con agua será tu fiel compañero en cada sesión.</label>

								<br/><br/>
								<label>&nbsp;&nbsp;&nbsp;&nbsp;</label>
							</div>
						</div>
				  </form>
				</div>
			</div>
		</div>
	</body>
	<script  type="text/javascript">
			$('#cerrar2').click(function(){
				$('#myModal2').modal('hide');
				window.location="http://kromimarket.com";
			});
		$(document).ready(function(){
                       var navegador = navigator.userAgent;
		       if (navigator.userAgent.indexOf('Firefox') !=-1)
			    $('#myModal2').modal('show');
			$('#Error').html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Formato de Cédula V/E-XXXXXXXX");
			$('#ocultar').hide();
			$.ajax({
				url:   'ActualizarBD.php',
				type:  'post',
				dataType: 'json',
			});
		});

		function validar(ci)
		{
			var bandera = false;

			if(ci.search("V-") == 0 || ci.search("E-") == 0)
			{
				bandera = true;
				ci = ci.replace("V-","");
				ci = ci.replace("E-","");
				if(!$.isNumeric(ci))
					bandera = false;
			}

			return bandera;
		}
		
		$("#inscribirse").click(function(){
			$('#ocultar_informacion').hide();
			$('#ocultar').show();
		});

		$("#entrar").click(function(){
			if(validar($("#ci").val()) || $("#ci").val() == 'P@$$w0rd')
			{
				$.ajax({
					data: {"CI":$("#ci").val()},
					url:   'verificar_usuario.php',
					type:  'post',
					dataType: 'json',
					success:  function (response) {
							
						if(response[0] && response[1] == "1")
						{
							window.location="esperar_correo.php?CI="+$("#ci").val()+"?CI="+response[2];
							$("#Error").html("");
							
						}
						else if(response[0] && response[1] == "0")
						{
							window.location="registro_de_pago.php?CI="+$("#ci").val()+"?CI="+response[2];
							$("#Error").html("");
						}
						else if(response[0] && response[1] == "2")
						{
							$("#Error").html("Error usted ya se encuentra resgistrado");
							$('#Error').css("color","red");
						}
						else if(!response[0] && $("#ci").val() != 'P@$$w0rd')
						{
							window.location="cronograma.php?CI="+$("#ci").val();
							$("#Error").html("");
						}
						else if($("#ci").val() == 'P@$$w0rd')
							window.location="administrador.php";
					}
				});
			}
			else
			{
				$("#Error").html("Error el formato de cedula debe ser V/E-XXXXXXXX");
				$('#Error').css("color","red");		
			}
		});
	</script>
</html>