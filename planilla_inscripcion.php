<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
     	<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- /////////////////////////////////////////CSS LIBRARIES/////////////////////////////////////////////////////////////////////////// -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
		<link rel="stylesheet" href="fondotraining.css">
   		<link href="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/d004434a5ff76e7b97c8b07c01f34ca69e635d97/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
   		<!-- /////////////////////////////////////////SCRIPTS LIBRARIES////////////////////////////////////////////////////////////////////// -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
		<script src="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/d004434a5ff76e7b97c8b07c01f34ca69e635d97/src/js/bootstrap-datetimepicker.js"></script>
	</head>
	<body>
		<div class="container">
			<br/><br/>
			<div>
				<img src="foto5.png" class="col-md-12" style="width:1137px;"/>
			</div>
			<label>&nbsp;&nbsp;&nbsp;&nbsp;<br/></label>
			<div class="col-xs-12">
				<form> 
					 <div class="panel panel-default" style="background-image: url('negro.jpg');">
					 	 <br/><br/><br/>
					 	 <div class="form-group">
			                <label class="col-xs-2 col-xs-offset-1" style="font: 120% sans-serif; color:white;">Nombres:</label>
			                <div class="col-xs-4 col-xs-pull-1">
			                    <input id="nombre" type="tel" class="form-control" name="nombre">
			                    <span id="errorNOMBRE" style="display: inline; font: 120% sans-serif;"></span>
			                </div>
			                <label class="col-xs-1 col-xs-pull-1" style="font: 120% sans-serif; color:white;">Apellidos:</label>
			                <div class="col-xs-4 col-xs-pull-1">
			                    <input id="apellido" type="tel" class="form-control" name="apellido">
			                    <span id="errorAPELLIDO" style="display: inline; font: 120% sans-serif;"></span>
			                </div>
			            </div>
			            <br/><br/><br/>

			            <div class="form-group">
			                <label class="col-xs-2 col-xs-offset-1" style="font: 120% sans-serif; color:white;">Cédula:</label>
			                <div class="col-xs-4 col-xs-pull-1">
			                    <input id="cedula" type="tel" class="form-control" name="cedula" readonly>
			                    <span id="errorCI" style="display: inline; font: 120% sans-serif;"></span>
			                </div>
			                <label class="col-xs-1 col-xs-pull-1" style="font: 120% sans-serif; color:white;">Correo:</label>
			                <div class="col-xs-4 col-xs-pull-1">
			                    <input id="email" type="tel" class="form-control" name="email">
			                    <span id="errorEMAIL" style="display: inline; font: 120% sans-serif;"></span>
			                </div>
			            </div>
			            <br/><br/><br/><br/>

			            <div class="form-group">
			                <label class="col-xs-2 col-xs-offset-1" style="font: 120% sans-serif; color:white;">Fecha de Nacimiento:</label>
			                <div class="col-xs-3">
			                    <div class="input-group date" id="datetimepicker">
			                        <input id="fech_nac" type="tel" class="form-control" name="fech_nac">
			                        <span class="input-group-addon">
			                            <span class="glyphicon glyphicon-calendar"></span>
			                        </span>
			                    </div>
			                    <span id="errorFECH_NAC" style="display: inline; font: 120% sans-serif;"></span>
			                </div>
			                <label class="col-xs-1" style="font: 120% sans-serif; color:white;">Sexo:</label>
			                <div class="col-xs-4">
			                    <select class="form-control" id="sexo" name="sexo">
			                    	<option>Indique su Opción</option>
			                        <option>M</option>
			                        <option>F</option>
			                    </select>
			                    <span id="errorSEXO" style="display: inline; font: 120% sans-serif;"></span>
			                </div>
			                <span id="errorSEXO" style="display: inline; font: 120% sans-serif;"></span>
			            </div>
			            <br/><br/><span id="espacios"></span><br/>

			            <div class="form-group">
			                <label class="col-xs-1 col-xs-offset-1" style="font: 120% sans-serif; color:white;">Ocupación:</label>
			                <div class="col-xs-3 col-xs-offset-1">
			                    <input id="ocupacion" type="tel" class="form-control" name="ocupacion">
			                    <span id="errorOCUPACION" style="display: inline; font: 120% sans-serif;"></span>
			                </div>
			                <label class="col-xs-2" style="font: 120% sans-serif; color:white;">Zona de residencia:</label>
			                <div class="col-xs-3">
			                	<input id="zona_res" type="tel" class="form-control" name="zona_res">
			                    <span id="errorZONA_RES" style="display: inline; font: 120% sans-serif;"></span>
			                </div>
			            </div>
			            <br/><br/><br/><br/>><br/>

			            <div class="form-group">
			                <label class="col-xs-1 col-xs-offset-1" style="font: 120% sans-serif; color:white;">Teléfono contacto:</label>
			                <div class="col-xs-3  col-xs-offset-1">
			                    <input id="tlf_residencia" type="tel" class="form-control" name="tlf_residencia">
			                </div>
			                <br/><br/>
			                <span id="errorTELEFONO" style="display: inline; font: 120% sans-serif;" class="col-xs-offset-1"></span>
			            </div>
			            <br/><br/><br/><br/>
					 	 <table class="table">
							<tr>
								<td class="col-xs-4">
								</td>
								<td class="col-xs-6">
									<button  id="guardar" type="button" class="col-xs-offset-2 col-xs-4" style="font: 120% sans-serif; color:gray;">Siguiente</button>
								</td>
								<td class="col-xs-4">
								</td>
							</tr>
						</table>
				         <br/><br/><br/>
					 </div>
				</form>
			</div>
			<div>
				<img src="foto1.png" class="col-md-12" style="width:1137px;"/>
			</div>
			<br/><br/><br/>
			<label>&nbsp;&nbsp;&nbsp;&nbsp;</label>
		</div>
	</body>
	<script  type="text/javascript">
		var URL = String(window.location);
		arregloDeSubCadenas = URL.split("?CI=");

		$(document).ready(function(){
			$('#errorTELEFONO').html("&nbsp;&nbsp;&nbsp;Formato del telefono xxxx-xxxxxxx");
			$('#errorTELEFONO').css("color","orange");
			$('#datetimepicker').datetimepicker({
					format: "DD/MM/YYYY"
			});
			$("#ocultar").hide();
			$('#cedula').val(arregloDeSubCadenas[1]);
			$('#ocultar_informacion').hide();
		});

		 function restaFechas(f1,f2)
		 {
			 var aFecha1 = f1.split('/'); 
			 var aFecha2 = f2.split('/'); 
			 var fFecha1 = Date.UTC(aFecha1[2],aFecha1[1]-1,aFecha1[0]); 
			 var fFecha2 = Date.UTC(aFecha2[2],aFecha2[1]-1,aFecha2[0]); 
			 var dif = fFecha2 - fFecha1;
			 var dias = Math.floor(dif / (1000 * 60 * 60 * 24)); 
			 return dias;
		 }

		function validar(nombres,apellidos,email,fecha_nac,telefono,sexo,zona_res,ocupacion)
		{


			var bandera = true;
		    var from = fecha_nac.split("/");
		    fecha = new Date(from[2],from[1]-1,from[0]);
		    fecha2 = new Date();

		    fechaAux = "";
		    fechaAux2 = "";
		    fechaAux = fechaAux+fecha.getDate()+"/"+(fecha.getMonth()+1)+"/"+fecha.getFullYear();
			fechaAux2 = fechaAux2+fecha2.getDate()+"/"+(fecha2.getMonth()+1)+"/"+fecha2.getFullYear();
			if(nombres == "" || nombres == " " || nombres.search("1") > 0 || nombres.search("2") > 0 || nombres.search("3") > 0 || nombres.search("4") > 0 || nombres.search("5") > 0 || nombres.search("6") > 0 || nombres.search("7") > 0 || nombres.search("8") > 0 || nombres.search("9") > 0 || nombres.search("0") > 0){
				$("#errorNOMBRE").html("Error el nombre no debe contener numeros ni ser vacio");
				$('#errorNOMBRE').css("color","red");
				$('#nombre').css("background","#f08080");
				bandera = false;
			}else
			{
				$("#errorNOMBRE").html("");
				$('#nombre').css("background","#90EE90");
			}

			
			if(apellidos == "" || apellidos == " " || apellidos.search("1") > 0 || apellidos.search("2") > 0 || apellidos.search("3") > 0 || apellidos.search("4") > 0 || apellidos.search("5") > 0 || apellidos.search("6") > 0 || apellidos.search("7") > 0 || apellidos.search("8") > 0 || apellidos.search("9") > 0 || apellidos.search("0") > 0){
				$("#errorAPELLIDO").html("Error el apellido no debe contener numeros ni ser vacio");
				$('#errorAPELLIDO').css("color","red");
				$('#apellido').css("background","#f08080");
				bandera = false;
			}else
			{
				$("#errorAPELLIDO").html("");
				$('#apellido').css("background","#90EE90");
			}
			
			if(email == "" || email == " " || (((email.split("@")).length < 2 || (email.split(".com")).length < 2) && ((email.split("@")).length < 2 || (email.split(".com.ve")).length < 2) && ((email.split("@")).length < 2 || (email.split(".net")).length < 2) && ((email.split("@")).length < 2 || (email.split(".COM")).length < 2) && ((email.split("@")).length < 2 || (email.split(".COM.VE")).length < 2) && ((email.split("@")).length < 2 || (email.split(".NET")).length < 2))) 
			{
				$("#errorEMAIL").html("Error el email no es valido");
				$('#errorEMAIL').css("color","red");
				$('#email').css("background","#f08080");
				bandera = false;
			}else
			{
				$("#errorEMAIL").html("");
				$('#email').css("background","#90EE90");
			}

			if(!(/^\d{4}-\d{7}$/).test(telefono)){
				$("#errorTELEFONO").html("Error el telefono debe ser este formato xxxx-xxxxxxx y no vacio");
				$('#errorTELEFONO').css("color","red");
				$('#tlf_residencia').css("background","#f08080");
				bandera = false;
			}else 
			{
				$("#errorTELEFONO").html("");
				$('#tlf_residencia').css("background","#90EE90");
			}
	
			if(ocupacion == "" || ocupacion == " " || ocupacion.search("1") > 0 || ocupacion.search("2") > 0 || ocupacion.search("3") > 0 || ocupacion.search("4") > 0 || ocupacion.search("5") > 0 || ocupacion.search("6") > 0 || ocupacion.search("7") > 0 || ocupacion.search("8") > 0 || ocupacion.search("9") > 0 || ocupacion.search("0") > 0){
				$("#errorOCUPACION").html("Error la ocupacion no debe contener numeros ni ser vacio");
				$('#errorOCUPACION').css("color","red");
				$('#ocupacion').css("background","#f08080");
				bandera = false;
			}else
			{
				$("#errorOCUPACION").html("");
				$('#ocupacion').css("background","#90EE90");
			}

			if(zona_res == "" || zona_res == " "){
				$("#errorZONA_RES").html("Error la zona de residencia no debe ser vacio");
				$('#errorZONA_RES').css("color","red");
				$('#zona_res').css("background","#f08080");
				bandera = false;
			}else
			{
				$("#errorZONA_RES").html("");
				$('#zona_res').css("background","#90EE90");
			}
			
			if(sexo == "Indique su Opción")
			{
				$("#errorSEXO").html("Error debe seleccionar una opcion");
				$('#errorSEXO').css("color","red");
				$('#sexo').css("background","#f08080");
				bandera = false;
			}
			else
			{
				$("#errorSEXO").html("");
				$('#sexo').css("background","#90EE90");
			}
			
			if(fecha_nac == "" || (fecha > fecha2) || /*(*/parseInt(restaFechas(fechaAux,fechaAux2)/365) <= 18/* && arregloDeSubCadenas[2] == 0)*/)
			{
				$("#espacios").html("<br/><br/><br/><br/>");
				if(parseInt(restaFechas(fechaAux,fechaAux2)/365) <= 18/* && arregloDeSubCadenas[2] == 0*/)
					$("#errorFECH_NAC").html("Error debe ser mayor de 18 años para cursar las lecciones elegidas");
				else
					$("#errorFECH_NAC").html("Error la fecha es invalida o es vacio");
				$('#errorFECH_NAC').css("color","red");
				$('#fech_nac').css("background","#f08080");
				bandera = false;
			}else
			{
				$("#espacios").html("");
				$("#errorFECH_NAC").html("");
				$('#fech_nac').css("background","#90EE90");
			}

			return bandera;
			
		}

		$("#guardar").click(function(){
			
if(validar($('#nombre').val(),$('#apellido').val(),$('#email').val(),$('#fech_nac').val(),$('#tlf_residencia').val(),$('#sexo').val(),$('#zona_res').val(),$('#ocupacion').val()))
			{
				var from = $('#fech_nac').val().split("/");
			    fecha = new Date(from[2],from[1]-1,from[0]);
			    cadena = fecha.getDate() + "/" + (fecha.getMonth() +1) + "/" + fecha.getFullYear();

				$.ajax({
					data: {"nombre":$('#nombre').val(),"apellido":$('#apellido').val(),"email":$('#email').val(),"fecha_nac":cadena,"zona_res":$('#zona_res').val(),"tlf_residencia":$('#tlf_residencia').val(),"cedula":$('#cedula').val(),"ocupacion":$('#ocupacion').val(),"sexo":$('#sexo').val()},
					url:   'guardar_inscripcion.php',
					type:  'post',
					dataType: 'json',
					success:  function (response) {
					}
				});
                                $.ajax({
					data:{"email":$('#email').val()},
					url:   'enviar_felicitaciones.php',
					type:  'post',
					dataType: 'json',
					success:  function (response) {
					}
				});
                                $.ajax({
					data: {"nombre":$('#nombre').val(),"apellido":$('#apellido').val(),"email":$('#email').val(),"fecha_nac":cadena,"zona_res":$('#zona_res').val(),"tlf_residencia":$('#tlf_residencia').val(),"cedula":$('#cedula').val(),"ocupacion":$('#ocupacion').val(),"sexo":$('#sexo').val()},
					url:   'enviar_datos_de_planilla.php',
					type:  'post',
					dataType: 'json',
					success:  function (response) {
					}
				});
				window.location="registro_de_pago.php?CI="+arregloDeSubCadenas[1]+"?CI="+arregloDeSubCadenas[3];
			}
		});
	</script>
</html>