-- phpMyAdmin SQL Dump
-- version 4.3.8
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 04-08-2016 a las 13:49:41
-- Versión del servidor: 5.5.48-37.8
-- Versión de PHP: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `kromi_training`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bancos`
--

CREATE TABLE IF NOT EXISTS `bancos` (
  `id` int(11) DEFAULT NULL,
  `nombre` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `bancos`
--

INSERT INTO `bancos` (`id`, `nombre`) VALUES
(1, 'Banco Caroni'),
(2, 'Banco Canarias de Venezuela'),
(3, 'Banco Confederado'),
(4, 'Bolivar Banco'),
(5, 'Corp Banca'),
(6, 'Banco de Credito de Colombia'),
(7, 'Banco del Caribe'),
(8, 'Banco Caroni'),
(9, 'Bancoro'),
(10, 'Banco de Venezuela'),
(11, 'Banco Sofitasa'),
(12, 'Banpro'),
(13, 'Banco Provincial'),
(14, 'Banesco'),
(15, 'Banco Fondo Comun'),
(16, 'Banco Occidental de Descuento'),
(17, 'Banco Venezolano de Credito'),
(18, 'Central'),
(19, 'Banco Guayana'),
(20, 'Banco Exterior'),
(21, 'Banco Industrial de Venezuela'),
(22, 'Banco Mercantil'),
(23, 'Banco Plaza'),
(24, 'Citibank'),
(25, 'Total Bank'),
(26, 'Instituto Municipal de Credito Popular'),
(27, 'Nuevo Mundo'),
(28, 'Banco Federal'),
(30, 'Casa Propia'),
(31, 'Del Sur'),
(32, 'Mi Casa'),
(1, 'Banco Caroni'),
(2, 'Banco Canarias de Venezuela'),
(3, 'Banco Confederado'),
(4, 'Bolivar Banco'),
(5, 'Corp Banca'),
(6, 'Banco de Credito de Colombia'),
(7, 'Banco del Caribe'),
(8, 'Banco Caroni'),
(9, 'Bancoro'),
(10, 'Banco de Venezuela'),
(11, 'Banco Sofitasa'),
(12, 'Banpro'),
(13, 'Banco Provincial'),
(14, 'Banesco'),
(15, 'Banco Fondo Comun'),
(16, 'Banco Occidental de Descuento'),
(17, 'Banco Venezolano de Credito'),
(18, 'Central'),
(19, 'Banco Guayana'),
(20, 'Banco Exterior'),
(21, 'Banco Industrial de Venezuela'),
(22, 'Banco Mercantil'),
(23, 'Banco Plaza'),
(24, 'Citibank'),
(25, 'Total Bank'),
(26, 'Instituto Municipal de Credito Popular'),
(27, 'Nuevo Mundo'),
(28, 'Banco Federal'),
(30, 'Casa Propia'),
(31, 'Del Sur'),
(32, 'Mi Casa');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `evento`
--

CREATE TABLE IF NOT EXISTS `evento` (
  `id` int(11) DEFAULT NULL,
  `Descripcion` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `evento`
--

INSERT INTO `evento` (`id`, `Descripcion`) VALUES
(1, 'Entrenamiento Funcional'),
(2, 'Entrenamiento GAP (Gluteos,Abdomen,Piernas)'),
(3, 'Get Fits Max (Auto Carga)'),
(4, 'Zumba Fits (Entrenamiento Familiar)');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fechaevento`
--

CREATE TABLE IF NOT EXISTS `fechaevento` (
  `id` int(11) DEFAULT NULL,
  `eventoId` int(11) DEFAULT NULL,
  `Fecha` varchar(45) CHARACTER SET utf8 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `fechaevento`
--

INSERT INTO `fechaevento` (`id`, `eventoId`, `Fecha`) VALUES
(1, 1, 'Domingo 21 de Agosto'),
(2, 2, 'Domingo 28 de Agosto'),
(3, 3, 'Domingo 04 de Septiembre'),
(4, 4, 'Domingo 11 de Septiembre');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Numeros`
--

CREATE TABLE IF NOT EXISTS `Numeros` (
  `Numero` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagos`
--

CREATE TABLE IF NOT EXISTS `pagos` (
  `id` int(11) DEFAULT NULL,
  `ParticipanteId` int(11) DEFAULT NULL,
  `nro_voucher` varchar(15) CHARACTER SET utf8 NOT NULL,
  `banco_orig` varchar(45) CHARACTER SET utf8 NOT NULL,
  `fecha_transf` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `transf_online` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `cedula` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `participanteevento`
--

CREATE TABLE IF NOT EXISTS `participanteevento` (
  `ParticipanteId` int(11) DEFAULT NULL,
  `EventoId` int(11) DEFAULT NULL,
  `Costo` decimal(18,2) DEFAULT NULL,
  `Pagado` decimal(18,2) DEFAULT NULL,
  `Id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `participanteevento`
--

INSERT INTO `participanteevento` (`ParticipanteId`, `EventoId`, `Costo`, `Pagado`, `Id`) VALUES
(1, 4, '3900.00', '3900.00', 4),
(1, 1, '1300.00', '1300.00', 1),
(1, 2, '2300.00', '2300.00', 2),
(1, 3, '3600.00', '3600.00', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `participantes`
--

CREATE TABLE IF NOT EXISTS `participantes` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) CHARACTER SET utf8 NOT NULL,
  `apellido` varchar(17) CHARACTER SET utf8 NOT NULL,
  `cedula` varchar(15) CHARACTER SET utf8 NOT NULL,
  `email` varchar(45) CHARACTER SET utf8 NOT NULL,
  `sexo` enum('M','F') CHARACTER SET utf8 NOT NULL,
  `fecha_nac` varchar(11) CHARACTER SET utf8 NOT NULL,
  `zona_res` varchar(45) CHARACTER SET utf8 NOT NULL,
  `tlf_res` char(12) CHARACTER SET utf8 NOT NULL,
  `ocupacion` varchar(45) CHARACTER SET utf8 NOT NULL,
  `ya_pago` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `participantes`
--

INSERT INTO `participantes` (`id`, `nombre`, `apellido`, `cedula`, `email`, `sexo`, `fecha_nac`, `zona_res`, `tlf_res`, `ocupacion`, `ya_pago`) VALUES
(1, 'Maria Gabriela', 'Madriz Garcia', 'V-12752892', 'gabybeicker@hotmail.com', 'F', '25/5/1976', 'MaÃ±ongo', '0412-4826936', 'Administradora', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
