<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
     	<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- /////////////////////////////////////////CSS LIBRARIES/////////////////////////////////////////////////////////////////////////// -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
		<link rel="stylesheet" href="fondotraining.css">
   		<link href="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/d004434a5ff76e7b97c8b07c01f34ca69e635d97/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
   		<!-- /////////////////////////////////////////SCRIPTS LIBRARIES////////////////////////////////////////////////////////////////////// -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
		<script src="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/d004434a5ff76e7b97c8b07c01f34ca69e635d97/src/js/bootstrap-datetimepicker.js"></script>
		<meta http-equiv="Expires" content="0" /> 
		<meta http-equiv="Pragma" content="no-cache" />

		<script type="text/javascript">
		  if(history.forward(1)){
		    location.replace( history.forward(1) );
		  }
		</script>
	</head>
	<body>
		<div class="container">
			<br/><br/>
			<div>
				<img src="foto5.png" class="col-md-12" style="width:1100px;"/>
				<label>&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/><br/></label>
			</div>
			<div id="ocultar">
<div>
				<img src="foto2.png" class="col-md-12" style="width:1100px;"/>
			</div>
			  <label>&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/><br/></label>
			</div>
                         <div class="col-xs-12" style="width:1100px;">
				<div class="panel panel-default" style="background-image: url('negro.jpg'); border-image: url('negro.jpg');">
					<br/><br/>
					<label class="col-xs-9" style="font: 150% sans-serif; color:yellow;  text-align: left;">Inscripción Exitosa</label>
					<br/><br/>
					<hr/>
					<table class="container">
						<thead>
						  <tr>
					      </tr>
					      <tr>
					        <th id="datos" class="col-xs-2"  style="font: 120% sans-serif; color:red; text-align: left;">Datos Personales:</th>

					      </tr>
					    </thead>
					</table>
					<table class="container">
						<thead>
						  <tr>
					      </tr>
					      <tr>
					        <th id="nombre1" class="col-xs-2"  style="font: 120% sans-serif; color:white; text-align: left;">Nombre: <span id="nombre"></span></th>
					        <th id="sexo1" class="col-xs-2"  style="font: 120% sans-serif; color:white; text-align: left;"><span id="espacios2"></span>Sexo: <span id="sexo"></span></th>
					      </tr>
					    </thead>
					</table>
					<table class="container">
						<thead>
						  <tr>
					      </tr>
					      <tr>
					        <th id="apellido1" class="col-xs-3"  style="font: 120% sans-serif; color:white; text-align: left;">Apellido: <span id="apellido"></span></th>
					        <th class="col-xs-3" style="font: 120% sans-serif; color:white; text-align: left;">Email: <span id="email"></span></th>
					      </tr>
					    </thead>
					</table>
					<table class="container">
						<thead>
						  <tr>
					      </tr>
					      <tr>
					        <th class="col-xs-3"  style="font: 120% sans-serif; color:white; text-align: left;">Fecha de Nacimiento: <span id="fecha_nac"></span></th>
					        <th class="col-xs-3"  style="font: 120% sans-serif; color:white; text-align: left;">Zona de Residencia: <span id="zona_res"></span></th>
					      </tr>
					    </thead>
					</table>
					<table class="container">
						<thead>
						  <tr>
					      </tr>
					      <tr>
					        <th class="col-xs-3"  style="font: 120% sans-serif; color:white; text-align: left;">Teléfono de Residencia: <span id="tlf_res"></span></th>
					        <th class="col-xs-3"  style="font: 120% sans-serif; color:white; text-align: left;">Ocupación: <span id="ocupacion"></span></th>
					      </tr>
					    </thead>
					</table>
					<table class="container">
						<thead>
						  <tr>
					      </tr>
					      <tr>
					        <th id="cedula1" class="col-xs-3"  style="font: 120% sans-serif; color:white; text-align: left;">Cédula: <span id="cedula"></span></th>
					      </tr>
					    </thead>
					</table>
					<br/><br/>
					<hr/>
					<table class="container">
						<thead>
						  <tr>
					      </tr>
					      <tr>
					        <th id="sesion" class="col-xs-3"  style="font: 120% sans-serif; color:red; text-align: left;">Sesiones de Entrenamiento:</th>
					      </tr>
					    </thead>
					</table>
					<span id="entrenamiento0"></span>
					<br/><br/>
					<hr/>
					<table class="container">
						<thead>
						  <tr>
					      </tr>
					      <tr>
					        <th id="pago" class="col-xs-3"  style="font: 120% sans-serif; color:red; text-align: left;">Datos del Pago:</th>
					      </tr>
					    </thead>
					</table>
					<table class="container">
						<thead>
						  <tr>
					      </tr>
					      <tr>
					        <th class="col-xs-2"  style="font: 120% sans-serif; color:white; text-align: left;">Nro de Voucher: <span id="nro_voucher"></span></th>
					        <th class="col-xs-2"  style="font: 120% sans-serif; color:white; text-align: left;">Banco de Origen: <span id="banco_orig"></span></th>
					      </tr>
					    </thead>
					</table>
					<table class="container">
						<thead>
						  <tr>
					      </tr>
					      <tr>
					        <th class="col-xs-2"  style="font: 120% sans-serif; color:white; text-align: left;">Fecha de Transferencia: <span id="fecha_transf"></span></th>
					        <th class="col-xs-2"  style="font: 120% sans-serif; color:white; text-align: left;">Tipo de Pago: <span id="transf_online"></span></th>
					      </tr>
					    </thead>
					</table>
					<br/><br/>
					<hr/>
					<label class="col-xs-9" style="font: 150% sans-serif; color:red; text-align: left;">MONTO PAGADO:&nbsp;&nbsp;BsF&nbsp;<span id="monto"></span></label>
					<br/><br/>
					<hr/>
					<br/><br/><br/>
					<div>
						<br/>
						<label class="col-xs-12 col-xs-offset-1" style="font: 150% sans-serif; color:white;"> Tips Kromi Training:</label>
						<br/><br/>
						<label class="col-xs-12 col-xs-offset-1" style="font: 120% sans-serif; color:white;">-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Protege tu piel de los rayos UV. Aplica protector solar antes de salir de casa.</label>
						<br/><br/>
						<label class="col-xs-12 col-xs-offset-1" style="font: 120% sans-serif; color:white;">-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Usar zapatos cómodos y flexibles. Tus pies te lo agradecerán.</label>
						<br/><br/>
						<label class="col-xs-12 col-xs-offset-1" style="font: 120% sans-serif; color:white;">-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Las prendas de vestir deben ser frescas y que te permitan moverte libremente.</label>
						<br/><br/>
						<label class="col-xs-12 col-xs-offset-1" style="font: 120% sans-serif; color:white;">-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Para ésta actividad debes mantenerte hidratado, por lo que  un cooler con agua será tu fiel compañero en cada sesión.</label>
						<br/><br/>
						<label class="col-xs-12 col-xs-offset-1" style="font: 120% sans-serif; color:white;">-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Y lo más importante: ¡Actitud positiva, mucha energía y ganas de pasarla bien! </label>
						<br/><br/>
						<hr/>
						<br/><br/>
						<label>&nbsp;&nbsp;&nbsp;&nbsp;</label>
					</div>
					<div class="form-group">
<div id="ocultar3">		                
<div class="col-xs-6">
		                      <input id="imprimir" class="col-xs-4 col-xs-offset-10" type="button" style="font: 120% sans-serif; color:gray;" value="Imprimir">
		                </div>
</div>
		            </div>
<div id="ocultar2">
					<br/><br/>
					<label>&nbsp;&nbsp;&nbsp;&nbsp;</label>
</div>
				</div>
			</div>
	</body>
	<script  type="text/javascript">
	var URL = String(window.location);
	arregloDeSubCadenas = URL.split("?CI=");

	$("#imprimir").click(function(){
                $("#datos").attr("class", "col-xs-1");
                $("#sesion").attr("class", "col-xs-1");
                $("#pago").attr("class", "col-xs-1");
                $("#nombre1").attr("class", "col-xs-1");
                $("#sexo1").attr("class", "col-xs-1 col-xs-offset-1");
                $("#cedula1").attr("class", "col-xs-1");
                $("#espacios2").html("&nbsp;&nbsp;");
                $("#tabla1").attr("class", "col-xs-1");
                $("#fecha1").attr("class", "col-xs-1");
                $("#tabla3").attr("class", "col-xs-1");
                $("#fecha3").attr("class", "col-xs-1"); 
                $("#fechaaux2").html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                $("#fechaaux3").html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                $("#fechaaux4").html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                $("#ocultar").hide();
                $("#ocultar2").hide();
                $("#ocultar3").hide();
		window.print();  
                $("#datos").attr("class", "col-xs-2");
                $("#sesion").attr("class", "col-xs-3");
                $("#pago").attr("class", "col-xs-3");
                $("#nombre1").attr("class", "col-xs-2");
                $("#sexo1").attr("class", "col-xs-2");
                $("#cedula1").attr("class", "col-xs-3");
                $("#espacios2").html("");
                $("#tabla1").attr("class", "col-xs-3");
                $("#fecha1").attr("class", "col-xs-3");
                $("#fecha2").attr("class", "col-xs-3");
                $("#tabla3").attr("class", "col-xs-3");
                $("#fecha3").attr("class", "col-xs-3");
                $("#fechaaux2").html("");
                $("#fechaaux3").html("");  
                $("#fechaaux4").html("");            
                $("#ocultar").show();
                $("#ocultar2").show();
                $("#ocultar3").show();

		$.ajax({
				data:  {"CI":arregloDeSubCadenas[1]},
				url:   'confirmado.php',
				type:  'post',
				dataType: 'json',
				beforeSend: function () {
				},
				success:  function (response) {
				}

		});
	});

	$(document).ready(function(){
                 $("#espacios").html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
		$.ajax({
				data:  {"CI":arregloDeSubCadenas[1]},
				url:   'llenar_planilla.php',
				type:  'post',
				dataType: 'json',
				beforeSend: function () {
				},
				success:  function (response) {
					$("#nombre").html(response[0].nombre);
					$("#apellido").html(response[0].apellido);
					$("#cedula").html(response[0].cedula);
					$("#email").html(response[0].email);
					$("#fecha_nac").html(response[0].fecha_nac);
					$("#zona_res").html(response[0].zona_res);
					$("#tlf_res").html(response[0].tlf_res);
					$("#ocupacion").html(response[0].ocupacion);
					$("#sexo").html(response[0].sexo);
					for(i = 0; i < response[1][0].contador; i++)
					{
						j = i+1;
if(response[1][i].evento == "Entrenamiento Funcional")
{
						$("#entrenamiento"+i).html("<table class='container'><thead><tr></tr><tr><th  id='tabla1' class='col-xs-3'  style='font: 120% sans-serif; color:white; text-align: left;'>"+response[1][i].evento+"</th><th id='fecha1' class='col-xs-3'  style='font: 120% sans-serif; color:white; text-align: left;'><span id='fechaaux1'></span>"+response[1][i].Fecha+"</th></tr></thead></table><span id='entrenamiento"+j+"'></span>");
}
if(response[1][i].evento == "Entrenamiento GAP (Gluteos,Abdomen,Piernas)")
{
						$("#entrenamiento"+i).html("<table class='container'><thead><tr></tr><tr><th  id='tabla2' class='col-xs-3'  style='font: 120% sans-serif; color:white; text-align: left;'>"+response[1][i].evento+"</th><th id='fecha2' class='col-xs-3'  style='font: 120% sans-serif; color:white; text-align: left;'><span id='fechaaux2'></span>"+response[1][i].Fecha+"</th></tr></thead></table><span id='entrenamiento"+j+"'></span>");
}
if(response[1][i].evento == "Get Fits Max (Auto Carga)")
{
						$("#entrenamiento"+i).html("<table class='container'><thead><tr></tr><tr><th  id='tabla3' class='col-xs-3'  style='font: 120% sans-serif; color:white; text-align: left;'>"+response[1][i].evento+"</th><th id='fecha3' class='col-xs-3'  style='font: 120% sans-serif; color:white; text-align: left;'><span id='fechaaux3'></span>"+response[1][i].Fecha+"</th></tr></thead></table><span id='entrenamiento"+j+"'></span>");
}
if(response[1][i].evento == "Zumba Fits (Entrenamiento Familiar)")
{
						$("#entrenamiento"+i).html("<table class='container'><thead><tr></tr><tr><th  id='tabla4' class='col-xs-3'  style='font: 120% sans-serif; color:white; text-align: left;'>"+response[1][i].evento+"</th><th id='fecha4' class='col-xs-3'  style='font: 120% sans-serif; color:white; text-align: left;'><span id='fechaaux4'></span>"+response[1][i].Fecha+"</th></tr></thead></table><span id='entrenamiento"+j+"'></span>");
}
					}
					$("#monto").html(response[0].total);
					$("#nro_voucher").html(response[2].nro_voucher);
					$("#banco_orig").html(response[2].banco_orig);
					$("#fecha_transf").html(response[2].fecha_transf);
					$("#transf_online").html(response[2].transf_online);
	
				}

		});
	});
	</script>
