<?php
	include "conexion.php";

	function inicializar(&$datos,$Nro)
	{
		for($i = 0; $i < $Nro; $i++)
		{
			$datos["dia1"] = "";
			$datos["dia2"] = "";
			$datos["dia3"] = "";
			$datos["dia4"] = "";
		}
	}

	$eventos = mysqli_query($conexion,"SELECT N.Numero,D1.nombre AS dia1,D2.nombre AS dia2,D3.nombre AS dia3,D4.nombre AS dia4
									   FROM Numeros N
									   LEFT JOIN
									   (SELECT  @rownum := @rownum + 1 AS part,a.nombre
									   FROM participantes a
									   INNER JOIN participanteevento b ON (a.id=b.ParticipanteId),
									   (SELECT @rownum := 0) R
									   WHERE ya_pago=2
									   AND eventoid=1) D1 ON N.Numero=D1.part
									   LEFT JOIN
									   (SELECT  @rownum2 := @rownum2 + 1 AS part,a.nombre
									   FROM participantes a
									   INNER JOIN participanteevento b ON (a.id=b.ParticipanteId),
									   (SELECT @rownum2 := 0) R
									   WHERE ya_pago=2
									   AND eventoid=2) D2 ON N.Numero=D2.part
									   LEFT JOIN
									   (SELECT  @rownum3 := @rownum3 + 1 AS part,a.nombre
									   FROM participantes a
									   INNER JOIN participanteevento b ON (a.id=b.ParticipanteId),
									   (SELECT @rownum3 := 0) R
									   WHERE ya_pago=2
									   AND eventoid=3) D3 ON N.Numero=D3.part
									   LEFT JOIN
									   (SELECT  @rownum4 := @rownum4 + 1 AS part,a.nombre
									   FROM participantes a
									   INNER JOIN participanteevento b ON (a.id=b.ParticipanteId),
									   (SELECT @rownum4 := 0) R
									   WHERE ya_pago=2
									   AND eventoid=4) D4 ON N.Numero=D4.part");

	inicializar($datos,mysqli_num_rows(mysqli_query($conexion, "SELECT N.Numero,D1.nombre AS dia1,D2.nombre AS dia2,D3.nombre AS dia3,D4.nombre AS dia4
															    FROM Numeros N
															    LEFT JOIN
															    (SELECT  @rownum := @rownum + 1 AS part,a.nombre
															    FROM participantes a
															    INNER JOIN participanteevento b ON (a.id=b.ParticipanteId),
															    (SELECT @rownum := 0) R
															    WHERE ya_pago=2
															    AND eventoid=1) D1 ON N.Numero=D1.part
															    LEFT JOIN
															    (SELECT  @rownum2 := @rownum2 + 1 AS part,a.nombre
															    FROM participantes a
															    INNER JOIN participanteevento b ON (a.id=b.ParticipanteId),
															    (SELECT @rownum2 := 0) R
															    WHERE ya_pago=2
															    AND eventoid=2) D2 ON N.Numero=D2.part
															    LEFT JOIN
															    (SELECT  @rownum3 := @rownum3 + 1 AS part,a.nombre
															    FROM participantes a
															    INNER JOIN participanteevento b ON (a.id=b.ParticipanteId),
															    (SELECT @rownum3 := 0) R
															    WHERE ya_pago=2
															    AND eventoid=3) D3 ON N.Numero=D3.part
															    LEFT JOIN
															    (SELECT  @rownum4 := @rownum4 + 1 AS part,a.nombre
															    FROM participantes a
															    INNER JOIN participanteevento b ON (a.id=b.ParticipanteId),
															    (SELECT @rownum4 := 0) R
															    WHERE ya_pago=2
															    AND eventoid=4) D4 ON N.Numero=D4.part")));

	$i = 0;

	while($row = mysqli_fetch_array($eventos))
	{
		$datos["dia1"] = $row['dia1'];
		$datos["dia2"] = $row["dia2"];
		$datos["dia3"] = $row["dia3"];
		$datos["dia4"] = $row["dia4"];
		$datos2[$i] = $datos; 
		$i++;
	}
	$i--;
	
	echo json_encode(array(
	            "draw"            => 1,
	            "recordsTotal"    => $i,
	            "recordsFiltered" => $i,
	            "data"            => $datos2
    ));
?>