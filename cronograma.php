<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
      	<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- /////////////////////////////////////////CSS LIBRARIES/////////////////////////////////////////////////////////////////////////// -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
		<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet">
		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
		<link rel="stylesheet" href="fondotraining.css">
		<!-- /////////////////////////////////////////SCRIPTS LIBRARIES////////////////////////////////////////////////////////////////////// -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
		<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
	</head>
	<body>
		<div class="container">
			<br/><br/>
			<div>
				<img src="foto5.png" class="col-md-12" style="width:1100px;"/>
				<label>&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/><br/><br/><br/><br/><br/><br/></label>
			</div>
			<div>
				<img src="foto2.png" class="col-md-12" style="width:1100px;"/>
			</div>
			<label>&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/></label>
			<div class="col-xs-4">
				<div class="form-group" style="width:1260px;">
					<div class="panel panel-default col-xs-2" style="width:1070px;  background-image: url('negro.jpg'); border-image: url('negro.jpg');">
						<table>
							<tr>
								<td class="col-xs-5">
									<br/>
									<label class="col-xs-12" style="font: 150% sans-serif; color:yellow;">Costo por cada sesión de entrenamiento:</label>
									<br/><br/><br/>
									<label class="col-xs-9 col-xs-offset-1" style="font: 120% sans-serif; color:white;">- 1 Clase - Bs 1.300,00</label>
									<br/><br/>
									<label class="col-xs-9 col-xs-offset-1" style="font: 120% sans-serif; color:white;">- 2 Clases - Bs 2.300,00</label>
									<br/><br/>
									<label class="col-xs-9 col-xs-offset-1" style="font: 120% sans-serif; color:white;">- 3 Clases - Bs 3.600,00</label>
									<br/><br/>
									<label class="col-xs-9 col-xs-offset-1" style="font: 120% sans-serif; color:white;">- 4 Clases - Bs 3.900,00</label>
									<br/>
								</td>
								<td>
									<div class="form-group">
					                    <img src="pesas1.jpg" class="col-xs-offset-2 col-md-4" style="border: white solid;">
					                    <img src="pesas3.jpg" class="col-md-4 col-xs-offset-1" style="height: 117px; border: white solid;">
						            </div>
								</td>
							</tr>
						</table>
						<br/>
						<label class="col-xs-10" style="font: 120% sans-serif; color:red;">Disfruta de una sesión "TOTALMENTE GRATIS": ¡ Inscríbete en 4 Sesiones y cancela solo 3 !</label><br/><br/><br/>
						<hr/>
						<table>
							<tr>
								<td class="col-xs-12">
									<label class="col-xs-11" style="font: 150% sans-serif; color:yellow;">Inscríbete a través de Transferencia o Depósito en efectivo:</label>
									<label class="col-xs-11" style="font: 120% sans-serif; color:yellow;"><i>(Debe realizar una transacción por persona)</i></label>
									<br/><br/>
									<label class="col-xs-9" style="font: 120% sans-serif; color:white;">a.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Realizar la transferencia o depósito a la cuenta:</label>
									<br/>
									<label class="col-xs-9" style="font: 120% sans-serif; color:white;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Banco Occidental de Descuento (Bod).</label>
									<br/>
									<label class="col-xs-10" style="font: 120% sans-serif; color:white;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0116-0001-84-0007120460.</label>
									<br/>
									<label class="col-xs-10" style="font: 120% sans-serif; color:white;">b.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A nombre de COMERCIALIZADORA KROMI MARKET CA.<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RIF J-30714575-7</label>
									<br/>
									<label class="col-xs-10" style="font: 120% sans-serif; color:white;">c.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ingresas a la página <a href="http://www.kromimarket.com/" target="_blank"><u>www.kromimarket.com</u></a></label>
									<br/>
									<label class="col-xs-10" style="font: 120% sans-serif; color:white;">d.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Completa tus datos en la PLANILLA DE INSCRIPCIÓN.</label>
									<br/>
									<label class="col-xs-12" style="font: 120% sans-serif; color:white;">e.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Marcar la opción Transferencia o Depósito y llenar los datos<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;correspondientes.</label>
									<br/>
									<label class="col-xs-12" style="font: 120% sans-serif; color:white;">f.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Coloca su correo electrónico para recibir la confirmación de la inscripción.</label>
									<br/><br/>
									<label>&nbsp;&nbsp;&nbsp;&nbsp;</label>
								</td>
								<td>
					                <img src="pesas2.jpg" class="col-md-12 col-xs-pull-6" style="height: 117px; width: 200px; border: white solid;">
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<div>
				<img src="foto1.png" class="col-md-12" style="width:1100px;"/>
			</div>
			<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
			<label>&nbsp;&nbsp;&nbsp;&nbsp;</label>
			<div class="col-xs-12" style="width:1100px;">
			<form>
			 <div class="panel panel-default" style="background-image: url('negro.jpg'); border-image: url('negro.jpg');">
			 	 <h2 class="col-xs-offset-1" style="font: 150% sans-serif; color:white;"><strong>&nbsp;&nbsp;Seleccione las fechas de su interes:</strong></h2>
			 	 <br/>
			 	 <div class="form-group">
			 	 	<label class="radio-inline col-xs-offset-1" style="font: 120% sans-serif; color:white;">
						  &nbsp;&nbsp;&nbsp;*&nbsp;Domingo 21 de Agosto "Entrenamiento Funcional" &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="radio1" id="opcion1"> 
					</label>
				 </div>
				 <div class="form-group">
					<label class="radio-inline col-xs-offset-1" style="font: 120% sans-serif; color:white;">
						  &nbsp;&nbsp;&nbsp;*&nbsp;Domingo 28 de Agosto "Entrenamiento GAP"  (Gluteos, Abdomen, Piernas) &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="radio2" id="opcion2"> 
					</label>
				 </div>
				 <div class="form-group">
					<label class="radio-inline col-xs-offset-1" style="font: 120% sans-serif; color:white;">
						  &nbsp;&nbsp;&nbsp;*&nbsp;Domingo 04 de Septiembre "Get Fit&nbsp; Max" &nbsp;(Auto Carga) &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="radio3" id="opcion3"> 
					</label>
				 </div>
				 <div class="form-group">
					<label class="radio-inline col-xs-offset-1" style="font: 120% sans-serif; color:white;">
						  &nbsp;&nbsp;&nbsp;*&nbsp;Domingo 11  de Septiembre  "Zumba &nbsp;Fit"   &nbsp;(Entrenamiento Familiar) &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="radio4" id="opcion4"> 
					</label>
				  </div>
				  <div class="form-group">
					<label class="col-xs-3 col-xs-offset-1" style="font: 120% sans-serif; color:white;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Monto a Pagar:</label>
		            <div class="col-xs-3">
		                <input id="monto" type="tel" class="form-control" value="0" readonly>
		            </div>
				</div>
				<br/><br/>
				<table>
					<tr>
                                                          
						<td class="col-xs-10">
							<label class="col-xs-offset-1" style="font: 150% sans-serif; color:white;"><strong>Recuerda que tu inscripción es un aporte a beneficio de la Fundación</strong></label>
						</td>
						<td>
			               <a href="https://www.instagram.com/llevamosalegria/

" target="_blank""><img src="foto6.png" style="width:230px;"/></a>
						</td>
						<td>
							<label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
						</td>
					</tr>
				</table>
				<span id="errorCRONOGRAMA" class="col-xs-offset-1" style="display: inline; font: 120% sans-serif; color:white;"></span>
			 	 <table class="table">
					<tr>
						<td class="col-xs-4">
						</td>
						<td class="col-xs-6">
							<div class="form-group">
								<button  id="guardar" type="button" class="col-xs-offset-2 col-xs-3" style="font: 120% sans-serif; color:gray;">Siguiente</button>
							</div>
						</td>
						<td class="col-xs-4">
						</td>
					</tr>
				</table>
		         <br/>
			 </div>
		</form>
		</div>
		</div>
	</body>
	<script  type="text/javascript">
		var cont1 = 0,cont2 = 0,cont3 = 0,cont4 = 0;
		var URL = String(window.location);
		arregloDeSubCadenas = URL.split("?CI=");

		$("#guardar").click(function(){
			var opcion1 = $('input:radio[id=opcion1]:checked').val();
			var opcion2 = $('input:radio[id=opcion2]:checked').val();
			var opcion3 = $('input:radio[id=opcion3]:checked').val();
			var opcion4 = $('input:radio[id=opcion4]:checked').val();
			
			if(opcion1 != "on" && opcion2 != "on" && opcion3 != "on" && opcion4 != "on")
			{
				$("#errorCRONOGRAMA").html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Error debe seleccionar al menos un evento");
				$('#errorCRONOGRAMA').css("color","red");
			}
			else
			{
				var opcion1 = $('input:radio[id=opcion1]:checked').val();
				var opcion2 = $('input:radio[id=opcion2]:checked').val();
				var opcion3 = $('input:radio[id=opcion3]:checked').val();
				var opcion4 = $('input:radio[id=opcion4]:checked').val();
				
				if(opcion1 != "on")
					opcion1 = "off";
				if(opcion2 != "on")
					opcion2 = "off";
				if(opcion3 != "on")
					opcion3 = "off";
				if(opcion4 != "on")
					opcion4 = "off";

				$.ajax({
					data: {"CI":arregloDeSubCadenas[1],"opcion1":opcion1,"opcion2":opcion2,"opcion3":opcion3,"opcion4":opcion4},
					url:   'guardar_cronograma.php',
					type:  'post',
					dataType: 'json',
				});

				if(opcion4 == "on")
					window.location="planilla_inscripcion.php?CI="+arregloDeSubCadenas[1]+"?CI="+"1"+"?CI="+$('#monto').val();
				else
					window.location="planilla_inscripcion.php?CI="+arregloDeSubCadenas[1]+"?CI="+"0"+"?CI="+$('#monto').val();
				$("#errorCRONOGRAMA").html("");
			}
		});

		setInterval('actualizar_monto()',10);
		function actualizar_monto()
		{
			var opcion1 = $('input:radio[id=opcion1]:checked').val();
			var opcion2 = $('input:radio[id=opcion2]:checked').val();
			var opcion3 = $('input:radio[id=opcion3]:checked').val();
			var opcion4 = $('input:radio[id=opcion4]:checked').val();
			
			if(opcion1 != "on")
				opcion1 = "off";
			if(opcion2 != "on")
				opcion2 = "off";
			if(opcion3 != "on")
				opcion3 = "off";
			if(opcion4 != "on")
				opcion4 = "off";
	
			if(opcion1 == "on" && opcion2 == "on" && opcion3 == "on" && opcion4 == "on")
			 	$('#monto').val("3900.00");
			else if((opcion1 == "on" && opcion2 == "on" && opcion3 == "on") || (opcion1 == "on" && opcion3 == "on" && opcion4 == "on") || (opcion2 == "on" && opcion3 == "on" && opcion4 == "on") || (opcion1 == "on" && opcion2 == "on" && opcion4 == "on"))
				$('#monto').val("3600.00");
			else if((opcion1 == "on" && opcion2 == "on") || (opcion1 == "on" && opcion3 == "on") || (opcion1 == "on" && opcion4 == "on") || (opcion2 == "on" && opcion3 == "on") || (opcion2 == "on" && opcion4 == "on") || (opcion3 == "on" && opcion4 == "on"))
				$('#monto').val("2300.00");
			else if(opcion1 == "on" || opcion2 == "on" || opcion3 == "on" || opcion4 == "on")
				$('#monto').val("1300.00");
			else if(opcion1 == "off" && opcion2 == "off" && opcion3 == "off" && opcion4 == "off")
				$('#monto').val("0.00");
		}

		$('#opcion1').click(function(){
			var opcion1 = $('input:radio[id=opcion1]:checked').val();
			if(opcion1 == "on" && cont1 == 1)
				$('#opcion1').prop("checked",false);
			if(cont1 == 1)
				cont1--;
			else
				cont1++;
		});

		$('#opcion2').click(function(){
			var opcion2 = $('input:radio[id=opcion2]:checked').val();
			if(opcion2 == "on" && cont2 == 1)
				$('#opcion2').prop("checked",false);
			if(cont2 == 1)
				cont2--;
			else
				cont2++;
		});

		$('#opcion3').click(function(){
			var opcion3 = $('input:radio[id=opcion3]:checked').val();
			if(opcion3 == "on" && cont3 == 1)
				$('#opcion3').prop("checked",false);
			if(cont3 == 1)
				cont3--;
			else
				cont3++;
		});

		$('#opcion4').click(function(){
			var opcion4 = $('input:radio[id=opcion4]:checked').val();
			if(opcion4 == "on" && cont4 == 1)
				$('#opcion4').prop("checked",false);
			if(cont4 == 1)
				cont4--;
			else
				cont4++;
		});
	</script>
</html>